-- Julio Lopez
-- Moondawgs Database

-- Survey Generator

--CREATE DATABASE
drop database if exists jjl18b_survey;
create DATABASE jjl18b_survey encoding 'UTF-8';
\c jjl18b_survey1;

--CREATE QUESTION TABLE
drop table if exists question;
create table question (
id int primary key not null,
text text,
question_type text);

insert into question (id, text, question_type) values
(1, 'I LOVE ACU!', 'Likert'),
(2, 'What is your classification?', 'Multiple Choice'),
(3, 'Hometown:', 'Type-in'),
(4, 'Expected Grad Year:', 'Type-in'),
(5, 'Why did you choose ACU?', 'Short Answer'),
(6, 'I am part of a student organization.', 'True/False'),
(7, 'I get a sufficient amount of work done.', 'Likert'),
(8, 'Purple is the best color in the world.', 'True/False'),
(9, 'How would you describe Dr. Reeves in a few words?', 'Short Answer'),
(10, 'Fill in the blank: Blee, ______, PURPLE!', 'Multiple Choice');

select *
from question;

--CREATE CHOICE TABLE
drop table if exists choice;
create table choice (
question_id int,
n int,
title text,
primary key (question_id, n)
);

insert into choice (question_id, n, title) values
(2,1, 'Freshman'),
(2,2, 'Sophomore'),
(2,3, 'Junior'),
(2,4, 'Senior'),
(10,1, 'Blee'),
(10,2, 'Blah'),
(10,3, 'Guh'),
(10,4, 'Purple');

select *
from choice;

--Choice to question relationships 
select q.id, q.text, q.question_type, c.question_id, c.n, c.title
from question as q
inner join choice as c
on q.id = c.question_id
order by q.id, c.question_id;

--CREATE SURVEY TABLE
drop table if exists survey;
create table survey (
id int primary key not null,
title text,
purpose text,
num_questions int,
author int);

insert into survey (id, title, purpose, num_questions, author) values
(1, 'ACU Students', 'Learn about the student body.', 6, 1),
(2, 'Should You Be Fired?', 'Get rid of the unproductives.', 3, 2),
(3, 'PURPLE!', 'Blee... Blah... just because.', 3, 2)
(4, '', , ),
(5, '', , ),
(6, '', , ),
(7, '', , ),
(8, '', , ),
(9, '', , ),
(10, '', , ),
(11, '', , ),
(12, '', , ),
(13, '', , ),
(14, '', , ),
(15, '', , );

select *
from survey;

--CREATE SURVEY_QUESTION REALTIONSHIP TABLE
drop table if exists survey_question;
create table survey_question (
survey_id int,
question_id int);

insert into survey_question (survey_id, question_id) values
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(2, 7),
(2, 8),
(2, 9),
(3, 8),
(3, 9),
(3, 10);

-- Survey to question relationships
select s.id, s.title, s.author, sq.survey_id, sq.question_id, q.id, q.text, q.question_type
from survey as s
inner join survey_question as sq
on s.id = sq.survey_id
inner join question as q
on sq.question_id = q.id
order by s.id, sq.survey_id;

--CREATE AUTHOR TABLE
drop table if exists author;
create table author (
id int primary key NOT NULL,
fn text,
ln text,
email text);

insert into author (id, fn, ln, email) values
(1, 'Phil', 'Schubert', 'p.schubs@acu.edu'),
(2, 'Brent', 'Reeves', 'brent@acu.edu');

select *
from author;

--CREATE USERS TABLE
drop table if exists users;
create table users (
id int primary key,
email text,
password text,
admin char);

-- ADD LOGIN POSSIBILITY FOR AUTHORS`
insert into users (id, email, password, admin) values
(1, 'xyz@acu.edu', '12345Acu', 'Y'),
(2, 'qrs@hsu.edu', '55555Hsu', 'N'),
(3, 'qwerty@acu.edu', 'funnysmell34', 'N'),
(4, 'employee1@reeves.com', 'happyardvark13!', 'N'),
(5, 'employee2@reeves.com', 'sillystring22!', 'N'),
(6, 'purple.iscool@purple.com', 'Purple493', 'Y')
(7, 'jjl18b@acu.edu', 'moonie56', 'Y'),
(8, 'abc@gmail.com', 'cringe', 'N'),
(9, 'phil.schubert@acu.edu', 'SCHUBB1', 'Y'),
(10, 'abs@gmail.com', 'cringe', 'N'),
(11, 'nice@gmail.com', 'cringe11', 'N'),
(12, 'nice1@gmail.com', 'cringe22', 'N'),
(13, 'drewpog@gmail.com', 'cringe33', 'N'),
(14, 'poggies@gmail.com', 'cringe44', 'N'),
(15, 'nice@acu.edu', 'cringe55', 'N'),
(16, 'nice1@acu.edu', 'cringe66', 'N'),
(17, 'test@gmail.com', 'cringe77', 'N'),
(18, 'test1@acu.edu', 'cringe88', 'N'),
(19, 'example@acu.edu', 'cringe99', 'N'),
(20, 'example1@gmail.com', 'cringe00', 'N'),
(21, 'google@gmail.com', 'cringe12', 'N'),
(22, 'eox@gmail.com', 'cringe23', 'N'),
(23, 'eox69@acu.edu', 'cringe34', 'N'),
(24, 'bir@gmail.com', 'cringe45', 'N'),
(25, 'owa@gmail.com', 'cringe56', 'N'),
(26, 'owaowa@acu.edu', 'cringe67', 'N'),
(27, 'nova19@acu.edu', 'cringe78', 'N'),
(28, 'nova20@acu.edu', 'cringe89', 'N'),
(29, 'nova21@acu.edu', 'cringe90', 'N'),
(30, 'Tammo@acu.edu', 'cringe21', 'N')
(31, 'moonielopez@acu.edu', 'acu111', 'N'),
(32, 'iss@gmail.com', 'acu222', 'N'),
(33, 'google@hotmail.com', 'acu333', 'N'),
(34, 'deez@gmail.com', 'acu444', 'N'),
(35, 'bruv@gmail.com', 'acu555', 'N'),
(36, 'lits@acu.edu', 'acu666', 'N'),
(37, 'fgv@acu.edu', 'acu777', 'N'),
(38, 'jere@gmail.com', 'acu888', 'N'),
(39, 'psql@acu.edu', 'acu999', 'N'),
(40, 'mysql@gmail.com', 'acu000', 'N');

select *
from users;

select *
from users
where admin = 'Y';

--query for specific user login
select id, email, password
from users
where email in ('xyz@acu.edu','qrs@hsu.edu','qwerty@acu.edu') and password = 'funnysmell34';

--CREATE USERS_SURVEY RELATIONSHIP TABLE
drop table if exists users_survey;
create table users_survey (
user_id int,
survey_id int);

insert into users_survey (user_id, survey_id) values
(1, 1),
(1, 3),
(2, 3),
(3, 1),
(4, 2),
(4, 3),
(5, 2),
(6, 3);

-- User to survey relationships
select u.id, u.email, us.user_id, us.survey_id, s.author, s.title, s.purpose, s.num_questions
from users as u
inner join users_survey as us
on u.id = us.user_id
inner join survey as s
on s.id = us.survey_id
order by u.id, us.survey_id;

--CREATE RESPONSE TABLE
drop table if exists response;
create table response (
id int primary key,
user_id int,
survey_id int,
question_id int,
answer text);

insert into response (id, user_id, survey_id, question_id, answer) values
(1, 1, 1, 1, 'Strongly Agree'),
(2, 2, 1, 2, 'Junior'),
(3, 3, 1, 3, 'Bandera, TX'),
(4, 4, 1, 4, '2022'),
(5, 5, 1, 5, 'Good press.'),
(6, 6, 1, 6, 'True'),
(7, 7, 1, 8, 'False'),
(8, 8, 1, 9, 'He is cool.'),
(9, 9, 1, 10, 'Guh'),
(10, 10, 1, 8, 'True'),
(11, 11, 1, 9, 'Good teacher.'),
(12, 12, 1, 10, 'Blah'),
(13, 13, 1, 1, 'Slightly Disagree'),
(14, 14, 1, 2, 'Senior'),
(15, 15, 1, 3, 'Savannah, GA'),
(16, 16, 2, 4, '2024'),
(17, 17, 2, 5, 'Family friend.'),
(18, 18, 2, 6, 'False'),
(19, 19, 2, 7, 'Slightly Agree'),
(20, 20, 2, 8, 'True'),
(21, 21, 2, 9, 'Types too fast.'),
(22, 22, 2, 8, 'False'),
(23, 23, 2, 9, 'Goofy goober.'),
(24, 24, 2, 10, 'Blee'),
(25, 25, 2, 7, 'Strongly Disagree'),
(26, 26, 2, 8, 'True'),
(27, 27, 2, 9, 'All around great guy.'),
(28, 28, 2, 8, 'True'),
(29, 29, 2, 9, 'Who is Dr. Reeves?'),
(30, 30, 2, 10, 'Guh'),
(31, 31, 3),
(32, 32, 3),
(33, 33),
(34, 34),
(35, 35),
(36, 36),
(37, 38),
(38, 39),
(39, 39),
(40, 40);

select *
from response
order by user_id, survey_id;

--See survey results
select r.id, r.user_id, u.id as user_id, u.email, r.survey_id, s.id as survey_id, s.title, r.question_id, q.id as question_id, q.text, r.answer
from response as r
inner join users as u
on u.id = r.user_id
inner join survey as s
on s.id = r.survey_id
inner join question as q
on q.id = r.question_id
order by r.user_id, r.survey_id;

drop table if exists url_link;
create table url_link (
url text primary key,
survey_id int,
question_id);
